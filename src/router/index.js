import Vue from "vue";
import Router from "vue-router";
//import AuthGuard from "./auth-quard";

import Home from "../views/Home.vue";
import About from "../views/About.vue";
import Standings from '../views/Standings.vue';
import Schedule from '../views/Schedule.vue'
import ScheduleResult from '../views/ScheduleResult.vue'

import NewsPage from '../views/News-page.vue'
import Players from '../views/Players.vue'
import Staff from '../views/Staff.vue'
import SingleNews from '../views/Single-news'
import News from '../views/News'
import Sponsors from '../views/Sponsors'
import SinglePlayer from '../views/PlayerNew'
import SingleStaff from '../views/SingleStaff'

import PrivacyPolicy from '../views/PrivacyPolicy'

import NewsSimple from '../views/NewsSimple'
//import NProgress from 'nprogress';


import NotFound from '../views/NotFound'

Vue.use(Router);

var router = new Router({
  //mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior: (to, from, savedPosition) => {
    return { x: 0, y: 0 };
  },
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
    },
    {
      path: '/404',
      name: '404',
      component: NotFound,
      meta: {
        layout: "simple-layout"
      }
    },
    {
      path: '*',
      redirect: '/404'
    },
    {
      path: "/news",
      name: "news",
      component: News
    },
    {
      path: '/news/video',
      name: 'news-simple',
      component: NewsSimple, props: { name: 'video' }
    },
    {
      path: '/news/photo',
      name: 'news-simple',
      component: NewsSimple, props: { name: 'photo' }
    },
    {
      path: "/news-page",
      name: "news-page",
      component: NewsPage
    },
    {
      path: "/standings",
      name: "standings",
      component: Standings
    },
    {
      path: "/schedule",
      name: "schedule",
      component: Schedule
    },
    {
      path: "/schedule-result/:id",
      name: "schedule-result",
      component: ScheduleResult
    },
    {
      path: "/players",
      name: "players",
      component: Players
    },
    {
      path: "/staff",
      name: "staff",
      component: Staff
    },
    {
      path: "/about",
      name: "about",
      component: About,
      //beforeEnter: AuthGuard
    },
    {
      path: "/sponsors",
      name: "sponsors",
      component: Sponsors,
      meta: {
        layout: "simple-layout"
      }
    },
    {
      path: "/single-news/:id",
      name: "single-news",
      component: SingleNews,
    },
    {
      path: "/player/:id",
      name: 'single-player',
      component: SinglePlayer
    },
    {
      path: "/staff/:id",
      name: 'single-staff',
      component: SingleStaff
    },
    {
      path: "/privacy-policy",
      name: 'privacy-policy',
      component: PrivacyPolicy, props: { name: 'privacy-policy' }
    },
    {
      path: "/legal-terms",
      name: 'legal-terms',
      component: PrivacyPolicy, props: { name: 'legal-terms' }
    }
  ]
});

/*router.beforeResolve((to, from, next) => {
  if (to.name) {
    NProgress.start()
    //store.commit("setLoading", true);
  }
  next()
})

router.afterEach((to, from) => {
  NProgress.done()
  //store.commit("setLoading", false);
  //document.querySelector(".home-hero").classList.add("in-viewport");
})
*/
export default router;
import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

import news from './modules/news';
import mainNews from './modules/mainNews'
import players from './modules/players'
import teams from './modules/teams'
import matches from './modules/matches'
import sponsors from './modules/sponsors'
import seasons from './modules/seasons'
import about from './modules/aboutPage'

export default new Vuex.Store({
  //strict: true,
  modules: {
    news,
    mainNews,
    players,
    teams,
    matches,
    sponsors,
    seasons,
    about
  },
  state: {
    layout: 'default-layout',
    mobile: false,
    loading: true,
    showModal: false,
    videoLink: '',
    tags: ["football", "bridgeportunitedfc", "sportinghartfordfc"],
    /*staff: [
      {
        "id": 1,
        "img": "img/staff.png",
        "name": " Robin",
        "secondName": "Johnson",
        "birthDay": "15 June 1986",
        "position": "Head Coach",
        "clubDebut": "10 May 2005",
        "country": {
          "title": "Ukraine",
          "code": "ua"
        },
        "h3": "Football is a family of team sports that involve, to varying degrees, kicking a ball to score a goal. ",
        "p": "Unqualified, the word football normally means the form of football that is the most popular where the word is used. Sports commonly called football include association football (known as soccer in some countries); gridiron football (specifically American football or Canadian football)"
      },
      {
        "id": 2,
        "img": "img/staff.png",
        "name": "Jake",
        "secondName": "Miligan",
        "birthDay": "15 June 1986",
        "position": "Assistant Coach",
        "clubDebut": "10 May 2005",
        "country": {
          "title": "Ukraine",
          "code": "ua"
        },
        "h3": "Football is a family of team sports that involve, to varying degrees, kicking a ball to score a goal. ",
        "p": "Unqualified, the word football normally means the form of football that is the most popular where the word is used. Sports commonly called football include association football (known as soccer in some countries); gridiron football (specifically American football or Canadian football)"
      },
      {
        "id": 3,
        "img": "img/staff.png",
        "name": " Jon",
        "secondName": "Medison",
        "birthDay": "15 June 1986",
        "position": "Manager",
        "clubDebut": "10 May 2005",
        "country": {
          "title": "Ukraine",
          "code": "ua"
        },
        "h3": "Football is a family of team sports that involve, to varying degrees, kicking a ball to score a goal. ",
        "p": "Unqualified, the word football normally means the form of football that is the most popular where the word is used. Sports commonly called football include association football (known as soccer in some countries); gridiron football (specifically American football or Canadian football)"
      },
      {
        "id": 4,
        "img": "img/staff.png",
        "name": " Dann",
        "secondName": "Carlton",
        "birthDay": "15 June 1986",
        "position": "Sales Manager",
        "clubDebut": "10 May 2005",
        "country": {
          "title": "Ukraine",
          "code": "ua"
        },
        "h3": "Football is a family of team sports that involve, to varying degrees, kicking a ball to score a goal. ",
        "p": "Unqualified, the word football normally means the form of football that is the most popular where the word is used. Sports commonly called football include association football (known as soccer in some countries); gridiron football (specifically American football or Canadian football)"
      }
    ],*/
  },
  actions: {
    determineResolution({ commit }, payload) {
      commit("determineResolution", payload)
    },
  },
  mutations: {
    setLayout(state, payload) {
      state.layout = payload
    },
    setLoading(state, payload) {
      state.loading = payload
    },
    showModal(state, payload) {
      state.showModal = payload
    },
    determineResolution(state, payload) {
      state.mobile = payload
    },
  },
  getters: {
    layout(state) {
      return state.layout
    },
    loading(state) {
      return state.loading;
    },
    showModal(state) {
      return state.showModal;
    },
    videoLink(state) {
      return state.videoLink
    },
    tags(state) {
      return state.tags;
    },
  },

})
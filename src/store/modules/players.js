export default {
    state: {
        players: [],
        staff: []
    },
    actions: {
        setPlayers({ commit }, payload) {
            commit("setPlayers", payload);
        },
    },
    mutations: {
        setPlayers(state, payload) {
            var players = [],
                staff = [];
            payload.map(el => {
                if (el.participantRole.name == 'Player') {
                    players.push(el)
                }
                else if ((el.participantRole.name == 'Staff')) {
                    staff.push(el)
                }
            })
            players.map((elem, index) => {
                elem.id = index + 1
            });
            state.players = players;
            state.staff = staff;
        },
    },
    getters: {
        players(state) {
            return state.players;
        },
        staff(state) {
            return state.staff
        }
    }
}
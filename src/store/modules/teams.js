export default {
    state: {
        teams: []
    },
    actions: {
        setTeams({ commit }, payload) {
            commit("setTeams", payload);
        },
    },
    mutations: {
        setTeams(state, payload) {
            state.teams = payload;
        },
    },
    getters: {
        teams(state) {
            return state.teams;
        },
    },


}
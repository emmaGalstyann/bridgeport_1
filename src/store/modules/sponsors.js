export default {
    state: {
        sponsors: []
    },
    actions: {
        setSponsors({ commit }, payload) {
            commit("setSponsors", payload);
        },
    },
    mutations: {
        setSponsors(state, payload) {
            state.sponsors = payload;
        },
    },
    getters: {
        sponsors(state) {
            return state.sponsors
        },
    }
}
import Vue from 'vue'

import router from "./router/";
import store from './store/';

import axios from 'axios'
import VueAxios from 'vue-axios'
import VueResource from 'vue-resource'

var VueScrollTo = require('vue-scrollto');

import App from './App.vue';


import './assets/reset.css';
import '../node_modules/slick-carousel/slick/slick.css';
import '../node_modules/nprogress/nprogress.css'
import "./assets/style.scss";
import './registerServiceWorker'


import inViewportDirective from 'vue-in-viewport-directive'
Vue.directive('in-viewport', inViewportDirective)

import DefaultLayout from './layouts/DefaultLayout.vue'
import SimpleLayout from './layouts/SimpleLayout.vue'

Vue.component('default-layout', DefaultLayout)
Vue.component('simple-layout', SimpleLayout)

import FlagIcon from 'vue-flag-icon'
Vue.use(FlagIcon);

import VModal from 'vue-js-modal'
Vue.use(VModal);

import VueStickyDirective from '@renatodeleao/vue-sticky-directive'
Vue.use(VueStickyDirective)

import VueYouTubeEmbed from 'vue-youtube-embed'
Vue.use(VueYouTubeEmbed)

var SocialSharing = require('vue-social-sharing');

Vue.use(SocialSharing);

Vue.use(VueScrollTo);
//Vue.use(VueAxios, axios);
Vue.use(VueResource);
Vue.http.options.root = 'http://localhost:3000/'

Vue.use(require('vue-moment'));

Vue.filter('reverse', function (value) {
  return value.reverse()
})

new Vue({
  el: '#app',
  router,
  //axios,
  store,
  render: h => h(App)
})
